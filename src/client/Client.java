/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import static client.Client.ping;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.ServerUtils;

/**
 *
 * @author
 * nhm95
 */
public class Client {

  public static DataOutputStream outToServer;
  public static BufferedReader inFromServer;
  public static Socket clientSocket;
  static boolean isSent = false;
  public static DataInputStream dataInputStream;

  static {

  }

  public static boolean ping() {
    try {
      String ping = "";
      outToServer.writeBytes("ping" + '\n');
      outToServer.flush();
      System.out.println("Ping .....");
      while (!ping.equals("OK")) {
        System.out.println("Pinging .....");
        Thread.sleep(1000);
        ping = dataInputStream.readLine();
        if (!ping.equals("OK")) {
          outToServer.writeBytes("ping" + '\n');
          outToServer.flush();
        }
      }
      System.out.println("Connected");
      return true;
    } catch (Exception ex) {
      return false;
    }
  }

  public static void init(String agrs[]) {
    try {
      String address = "127.0.0.1";
      if (agrs.length > 0) {
        address = agrs[0];
      }
      clientSocket = new Socket(address, 12345);
      System.out.println("Client " + clientSocket.getInetAddress() + " connected");
      ping();
    } catch (IOException ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public static void menu() {
    try {
      System.out.println("************************");
      System.out.println("1. upload");
      System.out.println("2. download");
      System.out.print("your action: ");
      String action = new Scanner(System.in).nextLine();
      switch (action) {
        case "1":
          upload();
          break;
        case "2":
          download();
          break;
      }
    } catch (Exception ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }
    menu();
  }

  public static void download() throws Exception {

    outToServer.writeBytes("download" + '\n');
    outToServer.flush();
    String lisFile = serverResponse();
    int index = 0;
    System.out.println("List files: ");
    for (String fileName : lisFile.split("#")) {
      index++;
      System.out.println(index + ". " + fileName);
    }
    System.out.print("Choose file name: ");
    String fileName = new Scanner(System.in).nextLine();
    outToServer.writeBytes(fileName + '\n');
    outToServer.flush();
    dataInputStream = new DataInputStream(clientSocket.getInputStream());
    String response;
    boolean isOk = true;
    response = dataInputStream.readLine();
    System.out.println("response ====> " + response);
    if (response.equals("OK")) {
      File file = new File("F:\\"+fileName);
      FileOutputStream fos = new FileOutputStream(file);
      ServerUtils.tranferFile(dataInputStream, fos);
//      byte[] buffer = new byte[1024];
//      int length;
//      int size = 0;
//      InputStream is = clientSocket.getInputStream();
//      int total = is.available();
//      System.out.println("total " + total);
//      while ((length = is.read(buffer)) > 0) {
//        size += length;
//        System.out.println("leng: " + length);
//        fos.write(buffer, 0, length);
//        System.out.println("size write: " + size);
//        if (length < buffer.length) {
//          fos.close();
//          break;
//        }
//      }
      System.out.println("download done!");
    }

  }

  public static void upload() throws Exception {
    outToServer.writeBytes("upload" + '\n');
    outToServer.flush();
    System.out.print("Enter file path: ");
    String filePath = new Scanner(System.in).nextLine();
    String fileName = filePath.substring(filePath.lastIndexOf("\\")+1, filePath.length());
    System.out.println(fileName);

    outToServer.writeBytes(fileName + '\n');
    outToServer.flush();
    File file = new File(filePath);
    FileInputStream fis = new FileInputStream(file);
    OutputStream os = clientSocket.getOutputStream();
    ServerUtils.tranferFile(fis, os);
  }

  public static void getChannels() throws Exception {
    outToServer.writeBytes("channels" + '\n');
    outToServer.flush();
    String json = dataInputStream.readLine();
  }

  public static void chat() throws Exception {
    outToServer.writeBytes("chat" + '\n');
    outToServer.flush();
    outToServer.writeBytes("test" + '\n');
    outToServer.flush();
    outToServer.writeBytes("Hello world !!!" + '\n');
    outToServer.flush();
    DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
    while (true) {
      String msg = dis.readLine();
      switch (msg) {
        case "chat":
          msg = dis.readLine();
          System.out.println("<" + clientSocket.getInetAddress().getHostName() + "> message: " + msg);
          break;
        case "tranfer":
          msg = dis.readLine();
          System.out.println("<" + clientSocket.getInetAddress().getHostName() + "> message: " + msg);
          break;
      }
    }
  }

  public static void main(String argv[]) {
    init(argv);

    try {
      outToServer = new DataOutputStream(clientSocket.getOutputStream());
      dataInputStream = new DataInputStream(clientSocket.getInputStream());
      if (ping()) {
        menu();
        chat();
      }
      clientSocket.close();
//      String sentence = "create";
//      String modifiedSentence;
//      outToServer = new DataOutputStream(clientSocket.getOutputStream());
//      dataInputStream = new DataInputStream(clientSocket.getInputStream());
//      if (ping()) {
//        outToServer.writeBytes("upload" + '\n');
//        outToServer.flush();
//
//        outToServer.writeBytes("CIS_Cisphone_User_Guide.docx" + '\n');
//        outToServer.flush();
//        File file = new File("C:\\Users\\nhm95\\Downloads\\CIS_Cisphone_User_Guide.docx");
//        FileInputStream fis = new FileInputStream(file);
//        byte[] buffer = new byte[1024];
//        int length;
//        while ((length = fis.read(buffer)) > 0) {
//          outToServer.write(buffer, 0, length);
//          outToServer.flush();
//        }

//      Thread.sleep(100000);
//      while (!(message.equalsIgnoreCase("stop"))) {
//        message = dis.readLine();
//        System.out.println("> "+message);
//      }
//      outToServer.writeBytes("unregister" + '\n');
//      outToServer.flush();
//      upload("F:\\Download\\test.iso");
//      clientSocket.close();
//        clientSocket.close();
//      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
  }

  public static String serverResponse() {
    String message = null;
    try {
      DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
      while ((message = dis.readLine()) == null) {
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return message;
  }

  public static void serverResponseFile() {
    try {
      String fileName = serverResponse();
      InputStream is = clientSocket.getInputStream();
      File file = new File("F:\\download\\" + fileName);
      FileOutputStream fos = new FileOutputStream(file);
      ServerUtils.tranferFile(is, fos);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void sendText(String text) {
    try {
      outToServer = new DataOutputStream(clientSocket.getOutputStream());
      outToServer.writeBytes(text + '\n');
      outToServer.flush();
      System.out.println("đã gửi");
      isSent = true;
    } catch (IOException ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
      isSent = false;
    }

  }

  public static void upload(String path) {
    try {
      InputStream is = null;
      OutputStream os = clientSocket.getOutputStream();
      File file = new File(path);
      if (file.exists()) {
        sendText(file.getName());
        while (!isSent) {
          System.out.println("sending .... !");
          Thread.sleep(2000);
        }
        is = new FileInputStream(file);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) > 0) {
          os.write(buffer, 0, length);
          os.flush();
        }
      }
      os.close();
      is.close();
    } catch (Exception ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }

  }

}
